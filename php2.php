<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
	<?php
	/*
	Nombre Alumno: Iván Rodríguez.
	Ejercicio: php2.php
	Enunciado: Haz un menú de tipo switch que no se vea afectado por el tipo de letra que introduzca el usuario (mayúsculas o minúsculas)
	Ejecución: Ha escogido la opción B.
	*/
	$variable="b";
	$variable = strtoupper($variable);
	switch($variable) {
	    case "A":
		echo "Ha escogido la opción A";
		break;
	    case "B":
		echo "Ha escogido la opción B";
		break;
	    case "C":
		echo "Ha escogido la opción C";
		break;
	    default:
		echo "El valor de la variable no es ni 1, ni 2, ni 3";
		break;	
	}
	?>
    </body>
</html>
