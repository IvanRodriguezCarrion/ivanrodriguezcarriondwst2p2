<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
	<?php
	/*
	Nombre Alumno: Iván Rodríguez.
	Ejercicio: php1.php
	Enunciado: Evalúa una variable numérica entre 1 y 3.
	Ejecución: El resultado es $variable.
	*/
	$variable=1;	
	
	if ($variable == 1) {
	    echo "El valor es $variable";
	} else if ($variable == 2) {
	    echo "El valor es $variable";
	} else if ($variable == 3) {
	    echo "El valor es $variable";
	} else {
	    echo "El valor de la variable no es ni 1, ni 2, ni 3";
	}
	?>
    </body>
</html>
