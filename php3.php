<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
	<?php
	/*
	Nombre Alumno: Iván Rodríguez.
	Ejercicio: php3.php
	Enunciado: Crea un script capaz de contar los caracteres presentes dentro de una palabra e imprimirlos por separado.
	Ejecución: El resultado es 4.
	*/
	$palabra="hola";
	
	for ($i=0; $i<strlen($palabra); $i++) {
	    echo "$palabra[$i] </br>";
	}
	
	echo "<p> El resultado es </p>" + strlen($palabra);
	?>
    </body>
</html>
